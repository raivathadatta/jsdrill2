// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*"

function getTheLastCar(data){
try {
    if(Array.isArray(data)){
        data.sort((car_a,car_b)=> {
            return car_a.car_year - car_b.car_year
        })
    
    return data[data.length-1]
    }else{
        throw "wrong format  expected array"
    }
} catch (error) {
 console.log(error)   
}
}

export default getTheLastCar