// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortTheInventoryByCarModel(data) {
    try {

        if (Array.isArray(data)) {
            data = data.sort((car_a, car_b) => car_a.car_model.localeCompare(car_b.car_model))
            return data
        }
        else {
            throw "wrong format expected array"
        }

    } catch (error) {
        console.log(error)
    }
}

export default sortTheInventoryByCarModel