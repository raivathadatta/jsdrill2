// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.



function getCarDetailsFromSpeficMakers(data, ...maker) {
    try {
        if(maker.length == 0){
            throw 'expecting atleast one parameter'
        }
        if (Array.isArray(data)) {
            maker = maker.map((car) => car.toLowerCase())
            let cars = data.filter((car) => maker.includes(car.car_make.toLowerCase()))
            return cars
        } else {
            throw "expected array"
        }
    } catch (error) {
        console.log(error)
    }

}

export default getCarDetailsFromSpeficMakers